package ftoop;

public class Drucker extends Thread {
	private Speicher speicher;

	Drucker(Speicher s) {
		this.speicher = s;
	}

	/**
	 * Holt einen Wert vom Zaehler und gibt ihn aus, gefolgt von einem einzelnen
	 * Leerzeichen.
	 * 
	 */
	@Override
	public void run() {
		while (true) {
			try {
				System.out.println("Drucker: " + speicher.getWert() + " ");
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			}
		}
	}
}
