package ftoop;

public class Speicher implements SpeicherIf {
	private int wert;
	private boolean hatWert;	
	
	@Override
	public synchronized int getWert() throws InterruptedException {
		// wait until the Z�hler has written a value and set the flag hatWert to true
		while (!hatWert){
			try{
				// wait until this thread gets signaled (with notifyAll()), that something has changed
				wait();
			}
			catch (InterruptedException e){
				Thread.currentThread().interrupt();
			}
		}
		hatWert = false;
		// notify every waiting thread (here: Z�hler) that something has changed
		notifyAll();
		return wert;
	}
	
	@Override	
	public synchronized void setWert(int wert) throws InterruptedException {
		// wait until the Drucker has read the value, and hatWert changed to false
		while (hatWert){
			try{
				// wait until this thread gets signaled (with notifyAll()), that something has changed
				wait();
			}
			catch(InterruptedException e){
				Thread.currentThread().interrupt();
			}
		}
		hatWert = true;
		// notify every waiting thread (here: Drucker) that something has changed
		notifyAll();
		this.wert = wert;
	}

	// this method is not used
	@Override
	public boolean getHatWert() {
		return hatWert;
	}
}
