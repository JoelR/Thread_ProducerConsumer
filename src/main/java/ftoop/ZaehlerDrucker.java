package ftoop;

public class ZaehlerDrucker {

	public static void main(String[] args) throws InterruptedException {
//		if (args.length != 2) {
//			System.out.println("Usage: ZaehlerDrucker <min> <max>");
//			System.exit(1);
//		}

		Speicher s = new Speicher();
		Drucker d = new Drucker(s);
		Zaehler z = new Zaehler(s, 1, 10);//new Zaehler(s, Integer.parseInt(args[0]), Integer.parseInt(args[1]));

		z.start();
		d.start();

		// bissi warten, damit der Test funktioniert
		Thread.sleep(5000);
	}

}
