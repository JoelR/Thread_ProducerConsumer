package ftoop;

import java.util.ArrayList;

public class SpeicherMock extends Speicher {
	
	private int wert;
	private boolean hatWert;
	private ArrayList<Integer> mocklist = new ArrayList<>();
	
	public SpeicherMock(){
		
	}

	@Override
	public int getWert() throws InterruptedException {
		return 0;
	}

	@Override
	public void setWert(int wert) throws InterruptedException {
		mocklist.add(wert);
		
	}

	@Override
	public boolean getHatWert() {
		return false;
	}
	
	public ArrayList<Integer> getList(){
		return mocklist;
	}
	

}
