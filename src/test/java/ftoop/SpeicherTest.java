package ftoop;

import static org.junit.Assert.*;
import org.junit.Test;

public class SpeicherTest {

	/**
	 * Test setter and getter functions. Both functions rely on each other, that's why they are tested together.
	 */
	@Test
	public void testGetAndSetWert() {
		Speicher sp = new Speicher();
		int value = 0;
		
		try {
			sp.setWert(15);
		}
		catch (InterruptedException e){
			System.out.println("Error thrown in method setWert().");
		}
		 try {
			 value = sp.getWert();
		}
		catch (InterruptedException e) {
			System.out.println("Error thrown in method getWert().");
		}
		assertEquals(15, value);
		// After calling getWert(), hatWert should be false.
		assertTrue(!sp.getHatWert());
	}

}
