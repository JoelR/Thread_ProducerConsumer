package ftoop;

import static org.junit.Assert.*;
import java.util.ArrayList;
import org.junit.Test;

public class ZaehlerTest {
	
	/**
	 * Tests the class Zaehler. Tests only the method setWert().
	 */
	
	//A list to compare to the result of zaehler
	private ArrayList<Integer> testlist = new ArrayList<>();
	
	public ZaehlerTest(){	//Produces a testlist containing {1,2,3,4,5,6,7,8,9,10}
		for(int i = 1; i <= 10; i++){
			testlist.add(i);
		}
	}	
	
    /**
	* Method which tests if a Zaehler object can be created. The SpeicherMock class is used to test the Speicher functionalities.
	* start() should count up and store the values in an arraylist, which should be equal the the list 'testlist' in the ZaehlerTest class when the test has finished.
	*/
	@Test
	public void testRun() {
		//The class SpeicherMock is used instead of the class Speicher
		SpeicherMock mock = new SpeicherMock();	
		int min = 1;
		int max = 10;
		Zaehler z = new Zaehler(mock, min, max);
		z.start();
		
		try {
			z.join();
		} catch (InterruptedException e) {
			fail("Error thrown in method z.join().");
		}
		
		//Tests if mocklist is the same as testlist and prints them on the console
		assertEquals(testlist, mock.getList());
		System.out.println("Testlist: ");
		System.out.println(testlist + "\n");
		System.out.println("Generated list: ");
		System.out.println(mock.getList());
	}


}
